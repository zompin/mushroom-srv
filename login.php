<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/config.php';
	session_start();

	if ($_SESSION['logged'] == true) {
		header('location: /');
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ($_POST['password'] == $password) {
			$_SESSION['logged'] = true;
			$_SESSION['login_error'] = '';
			header('location: /');
		} else {
			$_SESSION['login_error'] = 'Неверный пароль';
		}
	}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Авторизация</title>
	<style>
		html,
		body {
			height: 100%;
		}

		body {
			margin: 0;
			font-family: sans-serif;
		}

		.form {
			display: flex;
			justify-content: center;
			align-items: center;
			height: 100%;
		}
	</style>
</head>
<body>
	<div class="form">
		<form action="/login.php" method="POST" enctype="application-x/www-form-urlencoded">
			<p>Введи пароль всяк сюда входящий</p>
			<?php
				if ($_SESSION['login_error']) {
					echo '<p>' . $_SESSION['login_error'] . '</p>';
				}
			?>
			<input type="password" name="password" id="password">
			<input type="submit" value="Войти">
		</form>
	</div>
	<script>
		document.getElementById('password').focus();
	</script>
</body>
</html>
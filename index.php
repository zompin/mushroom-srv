<?php
	session_start();
	include $_SERVER["DOCUMENT_ROOT"] . "/version.php";

	if (!$_SESSION['logged']) {
		header('location: /login.php');
	}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Система поддержки микроклимата</title>
	<script src="/js/jquery-2.2.3.min.js"></script>
	<script src="/js/common.js"></script>
	<link rel="stylesheet" href="/style.css">
</head>
<body>
	<div id="wrapper">
		<div id="inner">
			<div class="main_ui">
				<div class="info_block version" title="<?php echo $update; ?>">Версия: <?php echo $version; ?></div>
				<div class="info_block free_memory"></div>
				<div class="info_block last_update_container"></div>
				<input type="button" value="Добавить помещение" class="add_room">
			</div>
			<div id="rooms"></div>
		</div>
	</div>
	<div class="form_wrapper">
		<div class="form_table">
			<div class="form_td">
				<div class="form_close"></div>
				<div class="room_form form">
					<span class="close_button">&times;</span>
					<div class="fields">
						<textarea class="room_description description" placeholder="Описание помещения"></textarea>
					</div>
					<div class="finish_buttons">
						<input type="button" value="Сохранить" id="save_room">
						<input type="button" value="Добавить" id="add_room">
					</div>
				</div>
				<div class="param_form form">
					<span class="close_button">&times;</span>
					<div class="fields">
						<p>
							Тип параметра:
							<select id="param_type">
								<option value="" checked>Выберите параметр</option>
								<option value="2">Температура</option>
								<option value="1">Влажность</option>
								<option value="3">Углекислый газ</option>
							</select>
						</p>
						<p>
							<label>Целевое значение параметра: <input type="text" id="target_value" placeholder="Введите целевое значение"></label>
						</p>
					</div>
					<div class="finish_buttons">
						<input type="button" value="Сохранить" id="save_param">
						<input type="button" value="Добавить" id="add_param">
					</div>
				</div>
				<div class="sensor_form form">
					<span class="close_button">&times;</span>
					<div class="fields">
						<p>
							Тип датчика:
							<select id="sensor_type">
								<option value="" checked>Выберите тип датчика</option>
								<option value="1">NTC</option>
								<option value="22">DHT22</option>
								<option value="2">DS18B20</option>
								<option value="3">MQ135</option>
							</select>
						</p>
						<p>
							<label>
								Номер порта: <input type="text" placeholder="Введите номер порта" id="sensor_port">
							</label>
						</p>
						<textarea class="sensor_description description" placeholder="Описание датчика"></textarea>
					</div>
					<div class="finish_buttons">
						<input type="button" value="Сохранить" id="save_sensor">
						<input type="button" value="Добавить" id="add_sensor">
					</div>
				</div>
				<div class="control_form form">
					<span class="close_button">&times;</span>
					<div class="fields">
						<p>
							Тип включения:
							<select id="control_type">
								<option value="" checked>Выберите тип включения</option>
								<option value="1">Ниже целевого значения</option>
								<option value="2">Выше целевого значения</option>
							</select>
						</p>
						<p>Номер порта: <input type="text" placeholder="Введите номер порта" id="control_port"></p>
						<div class="timer">
							<label class="timer_label">
								Таймер <input type="checkbox" class="timer_enable">
							</label>
							<div class="timer_time">
								Часов:<input type="text" class="timer_input timer_input_hours" value="0">
								Минут:<input type="text" class="timer_input timer_input_minutes" value="0">
								<label class="timer_label" title="Отчет начинается по достижению целевого значения">
									Отложенный отчет <input type="checkbox" class="timer_delay">
								</label>
							</div>
						</div>
						<textarea class="control_description description" placeholder="Введите описание управляемого устройства"></textarea>
					</div>
					<div class="finish_buttons">
						<input type="button" value="Сохранить" id="save_control">
						<input type="button" value="Добавить" id="add_control">
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
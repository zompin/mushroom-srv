var localData = {};
localData.room = {};
localData.param = {};
localData.sensor = {};
localData.control = {};
localData.timesspamps = null;

$(document).ready(function() {
	getInfo(true);
	$(".close_button, .form_close").click(closeForm);
	$("#rooms").delegate(".delete_room", "click", deleteRoom);
	$("#rooms").delegate(".delete_param", "click", deleteParam);
	$("#rooms").delegate(".delete_sensor", "click", deleteSensor);
	$("#rooms").delegate(".delete_control", "click", deleteControl);
	$(".add_room").click(showRoomAddForm);
	$("#rooms").delegate(".edit_room", "click", showRoomEditForm);
	$("#add_room").click(addRoom);
	$("#save_room").click(updateRoom);
	$("#rooms").delegate(".add_param", "click", showParamAddForm);
	$("#rooms").delegate(".edit_param", "click", showParamEditForm);
	$("#add_param").click(addParam);
	$("#save_param").click(updateParam);
	$("#rooms").delegate(".add_sensor", "click", showSensorAddForm);
	$("#rooms").delegate(".edit_sensor", "click", showSensorEditForm);
	$("#add_sensor").click(addSensor);
	$("#save_sensor").click(updateSensor);
	$("#rooms").delegate(".add_control", "click", showControlAddForm);
	$("#rooms").delegate(".edit_control", "click", showControlEditForm);
	$("#add_control").click(addControl);
	$("#save_control").click(updateControl);
	$(".timer_enable").click(togleTimer);
	$("#rooms").delegate(".ui", "click", function() {
		return false;
	});
	/*$(".form").click(function() {
		return false;
	});*/
});

// Получает данные с сервера и выводит всю структуру (помещения, парамаетры, датчики и контролы) на страницу
// Аргументы:
// recurse - указывает нужно ли делать запрос рекурсивно
function getInfo(recurse) {
	ajaxGET("action.php?action=getInfo", function(data) {
		var structure 	= data.structure;
		var timeStamps 	= data.timeStamps;
		var freeMemory 	= data.freeMemory;
		var lastUpdate 	= timeStamps.timestamp - timeStamps.last_update;

		buildStructure(structure);
		$(".last_update_container").html(lastUpdate);
		$('.free_memory').html(freeMemory + " / 8192");

		if (recurse) {
			setTimeout(function() {
				getInfo(true);
			}, 1000);
		}
	}, function() {
		setTimeout(function() {
			getInfo(true);
		}, 5000);
	});
}

// Производит POST запрос
// Аргументы:
// options  - параметры запроса
// callback - функция обратного вызова, которая вызывается после успешного выполения запроса
function ajaxPOST(options, callback) {
	var ajaxOptions 		= {};
	ajaxOptions.method 		= "POST";
	ajaxOptions.dataType 	= "JSON";
	ajaxOptions.url 		= "/action.php"

	if (callback !== undefined) {
		ajaxOptions.success = function(data) {
			callback(data);
		}
	}

	if (options !== null && options !== undefined) {
		if (options.data) {
			ajaxOptions.data = options.data;
		}

		if (options.url) {
			ajaxOptions.url = options.url;
		}
	}

	$.ajax(ajaxOptions);
}

// Производит GET запрос
// Аргументы:
// url 		- ссылка на страницу, с которой запрашивается страница
// callback - функция обратного вызова, которая вызывается после успешного выполения запроса
function ajaxGET(url, callback, onerror) {
	if (url === null || url === undefined)  {
		url = '/action.php';
	}

	$.ajax({
		url: url,
		dataType: "JSON",
		cache: false,
		success: function(data) {
			if (callback !== undefined) {
				callback(data);
			}
		},
		error: function(e) {
			if (onerror) {
				onerror();
			}
		}
	});
}

// Подготавливает структуру для вывода
// Аргументы:
// data - данные с сервера
function buildStructure(data) {
	var roomsE 	= $("#rooms");
	var room 	= "";

	if (data === null || data.length === 0) {
		room = "<div class=\"room_container\"><div class=\"room\">Нет помещений</div></div>";
	} else {
		for (var i = 0; i < data.length; i++) {
			room += "<div class=\"room_container\">"
			room += "<div class=\"room\" data-room-id=\"";
			room += data[i].id;
			room += "\" ";
			room += "data-room-description=\"" + data[i].description + "\""
			room += ">";
			room += data[i].description;
			room += getRoomUI();
			room += "</div>";
			room += buildParams(data[i].params);
			room += "</div>";
		}
	}

	$(roomsE).html(room);
}

// Подготавливают параметры для вывода
// Аргументы:
// params массив с параметрами
function buildParams(params) {
	var param = "";

	if (params.length == 0) {
		param += "<div class=\"param_container\">";
		param += "<div class=\"param\">";
		param += "Нет параметров";
		param += "</div>";
		param += "</div>";
	} else {

		for (var i = 0; i < params.length; i++) {
			param += "<div class=\"param_container\">";
			param += "<div class=\"param " + getParamClassByType(params[i].type) + "\" data-param-id=\"" + params[i].id + "\" ";
			param += "data-param-type=\"" + params[i].type + "\" ";
			param += "data-param-target-value=\"" + (params[i].target_value * 1).toFixed(2) + "\"";
			param += ">";
			param += "Параметр ";
			param += "Тип: " + paramTypeToString(params[i].type) + " ";
			param += "Целевое значение: "
			param += (params[i].target_value * 1).toFixed(2) + " ";
			param += "Текущее значение: " + (params[i].current_value * 1).toFixed(2);
			param += getParamUI();
			param += "</div>";
			param += buildSensors(params[i].sensors);
			param += buildControls(params[i].controls);
			param += "</div>";
		}

	}

	return param;
}

// Подготавливает датчики для вывода
// Аргументы:
// sensors - массив с датчиками
function buildSensors(sensors) {
	var sensor = "";

	if (sensors.length == 0) {
		sensor += "<div class=\"sensor_container\">";
		sensor += "<div class=\"sensor_header\">";
		sensor += "Нет датчиков";
		sensor += "</div>";
		sensor += "</div>";
	} else {
		sensor += "<div class=\"sensor_container\">";
		sensor += "<div class=\"sensor_header\">Датчики</div>";
		for (var i = 0; i < sensors.length; i++) {
			sensor += "<div class=\"sensor\" data-sensor-id=\"" + sensors[i].id + "\" ";
			sensor += "data-sensor-type=\"" + sensors[i].type + "\" ";
			sensor += "data-sensor-port=\"" + sensors[i].port + "\" ";
			sensor += "data-sensor-description=\"" + sensors[i].description + "\"";
			sensor += ">";
			sensor += "Датчик ";
			sensor += "Тип: ";
			sensor += sensorTypeToString(sensors[i].type) + " ";
			sensor += "Текущее значение: ";
			sensor += (sensors[i].value * 1).toFixed(2) + " ";
			sensor += "Порт: ";
			sensor += sensors[i].port + " ";
			sensor += sensors[i].description;
			sensor += getSensorUI();
			sensor += "</div>";
		}
		sensor += "</div>";
	}
	return sensor;
}

// Подготавливает контролы для вывода
// Аргументы:
// controls - массив с контролами
function buildControls(controls) {
	var control = "";

	if (controls.length == 0) {
		control += "<div class=\"control_container\">";
		control += "<div class=\"control_header\">";
		control += "Нет устройств";
		control += "</div>";
		control += "</div>";
	} else {
		control += "<div class=\"control_container\">";
		control += "<div class=\"control_header\">Устройства</div>";
		for (var i = 0; i < controls.length; i++) {
			control += "<div class=\"control\" data-control-id=\"" + controls[i].id + "\" ";
			control += "data-control-type=\"" + controls[i].type + "\" ";
			control += "data-control-port=\"" + controls[i].port + "\" ";
			control += "data-control-description=\"" + controls[i].description + "\" ";
			control += "data-control-timer-delay=\"" + controls[i].timerDelay + "\" ";
			control += "data-control-timer-enable=\"" + controls[i].timerEnable + "\" ";
			control += "data-control-timer-hours=\"" + controls[i].timerHours + "\" ";
			control += "data-control-timer-minutes=\"" + controls[i].timerMinutes + "\"";
			control +=">";
			control += "Тип: ";
			control += controlTypeToString(controls[i].type) + " ";
			control += "Порт: ";
			control += controls[i].port;
			control += getControlState(controls[i].current_state) + " ";
			control += getTimer(controls[i]);
			control += controls[i].description;
			control += getControlUI();
			control += "</div>";
		}
		control += "</div>";
	}
	return control;
}

// Возвращает тип параметра в виде строки
// Аргументы:
// type - тип параметра, в виде числа
function paramTypeToString(type) {
	var str;
	type *= 1;

	switch (type) {
		case 1:
			str = "Влажность";
		break;
		case 2:
			str = "Температура";
		break;
		case 3:
			str = "Углекислый газ";
		break;
	}

	return str;
}

// Возвращает тип датчика в виде строки
// Аргументы:
// type - тип датчика, в виде числа
function sensorTypeToString(type) {
	var str;

	type *= 1;
	switch (type) {
		case 1:
			str = "NTC";
		break;
		case 2:
			str = "DS18B20";
		break;
		case 3:
			str = "MQ-135";
		break;
		case 22:
			str = "DHT22";
		break;
	}

	return str;
}

// Возвращает тип контрола в виде строки
// Аргументы:
// type - тип контрола, в виде числа
function controlTypeToString(type) {
	var str;
	type *= 1;

	switch (type) {
		case 1:
			str = "Ниже целевого значения";
		break;
		case 2:
			str ="Выше целевого значения";
		break;
	}

	return str;
}

// Возвращает HTML-код интерфейса помещений
function getRoomUI() {
	var ui = "";
	ui += "<div class=\"ui\">";
	ui += "<button class=\"button edit_button edit_room\" title=\"Редактировать помещение\"></button>";
	ui += "<button class=\"button add_button add_param\" title=\"Добавить парметр\"></button>";
	ui += "<button class=\"button delete_button delete_room\" title=\"Удалить помещение\"></button>";
	ui += "</div>";

	return ui;
}

// Возвращает HTML-код интерфейса параметров
function getParamUI() {
	var ui = "";

	ui += "<div class=\"ui\">";
	ui += "<button class=\"button edit_button edit_param\" title=\"Редактировать параметр\"></button>";
	ui += "<button class=\"button add_button add_sensor\" title=\"Добавить датчик\"></button>";
	ui += "<button class=\"button add_button add_control\" title=\"Добавить устройство\"></button>";
	ui += "<button class=\"button delete_button delete_param\" title=\"Удалить параметр\"></button>";
	ui += "</div>";

	return ui;
}

// Возвращает HTML-код интерфейса датчиков
function getSensorUI() {
	var ui = "";

	ui += "<div class=\"ui\">";
	ui += "<button class=\"button edit_button edit_sensor\" title=\"Редактировать датчик\"></button>";
	ui += "<button class=\"button delete_button delete_sensor\" title=\"Удалить датчик\"></button>";
	ui += "</div>"

	return ui;
}

// Возвращает HTML-код интерфейса помещений
function getControlUI() {
	var ui = "";

	ui += "<div class=\"ui\">";
	ui += "<button class=\"button edit_button edit_control\" title=\"Редактировать устройство\"></button>";
	ui += "<button class=\"button delete_button delete_control\" title=\"Удалить устройство\"></button>";
	ui += "</div>";

	return ui;
}

// Возвращает элемент, который отобржает состояние контрола
// Аргументы:
// state - состояние контрола
function getControlState(state) {
	var className = '';

	if (state == 1) {
		className = "control_work";
	} else {
		className = "control_idle";
	}

	return "<span class=\"control_state " + className + "\"></span>";
}

// Возвращает элемент, в котором отображается время
// Аргументы:
// control - объект контрола
function getTimer(control) {
	var timer = "";

	if (control.timerEnable * 1) {
		timer += " <span class=\"control_timer\">";
		timer += formatTime(control.timerHours);
		timer += ":";
		timer += formatTime(control.timerMinutes);
		timer += ":";
		timer += formatTime(control.timerSeconds);
		timer += "</span> ";
	} 

	return timer;
}

// Добавляет ведущий ноль при необходимости
function formatTime(number) {
	number = number + "";

	if (number.length == 1) {
		number = "0" + number;
	}

	return number;
}

// Показывает сообщение, пока отладочное
// Аргументы:
// msg - сообщение, которое нужно показать
function showMessage(msg) {
	console.log(msg);
}

// Удаляет помещение из базы
function deleteRoom() {
	var roomId 	= $(this).parent().parent().attr("data-room-id");
	var res 	= confirm("Вы действительно хотите удалить помещение");

	if (!res) return;

	ajaxPOST({
		data: {
			id: roomId, 
			action: "delete_room"
		}
	}, showMessage);
}

// Удаляет параметр из базы
function deleteParam() {
	var paramId = $(this).parent().parent().attr("data-param-id");
	var res 	= confirm("Вы действительно хотите удалить параметр");

	if (!res) return;

	ajaxPOST({
		data: {
			id: paramId,
			action: "delete_param"
		}
	});
}

// Удаляет датчик из базы
function deleteSensor() {
	var sensorId 	= $(this).parent().parent().attr("data-sensor-id");
	var res 		= confirm("Вы действительно хотите удалить датчик");

	if (!res) return;

	ajaxPOST({
		data: {
			id: sensorId,
			action: "delete_sensor"
		}
	});
}

// Удаляет контрол из базы
function deleteControl() {
	var controlId 	= $(this).parent().parent().attr("data-control-id");
	var res 		= confirm("Вы действительно хотите удалить устройство");

	if (!res) return;

	ajaxPOST({
		data: {
			id: controlId,
			action: "delete_control"
		}
	});
}

// Добавляет помещение в базу
function addRoom() {
	var description = $(".room_description").val();

	ajaxPOST({
		data: {
			description: description,
			action: "add_room"
		}
	}, function(data) {
		closeForm();
		getInfo();
		if (data) {
			console.log(data);
		}
	});
}

// Добавляет параметр в базу
function addParam() {
	var roomId 		= localData.param.roomId;
	var type 		= $("#param_type").val();
	var targetValue = $("#target_value").val();

	ajaxPOST({
		data: {
			"room_id": roomId,
			"type": type,
			"target_value": targetValue,
			"action": "add_param"
		}
	}, function(data) {
			closeForm();
			getInfo();
		if (data) {
			console.log(data);
		}
	});
}

// Добавляет датчик в базу
function addSensor() {
	var paramId 		= localData.sensor.paramId;
	var type 			= $("#sensor_type").val();
	var port 			= $("#sensor_port").val();
	var description 	= $('.sensor_description').val();

	ajaxPOST({
		data: {
			"param_id": paramId,
			"type": type,
			"port": port,
			"description": description,
			"action": "add_sensor"
		}
	}, function(data) {
		closeForm();
		getInfo();
		if (data) {
			console.log(data);
		}
	});
}

// Добавляет контрол в базу
function addControl() {
	var paramId 		= localData.control.paramId;
	var type 			= $("#control_type").val();
	var port 			= $("#control_port").val();
	var description 	= $(".control_description").val();
	var timerDelay 		= $(".timer_delay").prop("checked") * 1;
	var timerEnable 	= $(".timer_enable").prop("checked") * 1;
	var timerHours 		= $(".timer_input_hours").val();
	var timerMinutes 	= $(".timer_input_minutes").val();

	if (!timerEnable) {
		timerHours 		= 0;
		timerMinutes 	= 0;
	}

	ajaxPOST({
		data: {
			"param_id": paramId,
			"type": type,
			"port": port,
			"description": description,
			"timerDelay": timerDelay,
			"timerEnable": timerEnable,
			"timerHours": timerHours,
			"timerMinutes": timerMinutes,
			"action": "add_control"
		}
	}, function(data) {
		closeForm();
		getInfo();
		if (data) {
			console.log(data);
		}
	});
}

// Обновляет данные о помещении
function updateRoom() {
	var roomId 		= localData.room.id;
	var description = $(".room_description").val();
	var keys 		= ["description"];
	var values 		= [description];

	ajaxPOST({
		data: {
			"keys": keys,
			"values": values,
			"room_id": roomId,
			"action": "update_room"
		}
	}, function(data) {
		closeForm();
		if (data) {
			console.log(data);
		}
	});
}

// Обновляет данные о параметре
function updateParam() {
	var paramId 	= localData.param.id //todo получить id парметра
	var paramType 	= $("#param_type").val();
	var targetValue = $("#target_value").val();
	var keys 		= ["type", "target_value"];
	var values 		= [paramType, targetValue];

	ajaxPOST({
		data: {
			"keys": keys,
			"values": values,
			"param_id": paramId,
			"action": "update_param"
		}
	}, function(data) {
		closeForm();
		if (data) {
			console.log(data)
		}
	});
}

// Обновляет данные о датчике
function updateSensor() {
	var sensorId 	= localData.sensor.id;
	var sensorType 	= $("#sensor_type").val();
	var sensorPort 	= $("#sensor_port").val();
	var description = $(".sensor_description").val();
	var keys 		= ["type", "port", "description"];
	var values 		= [sensorType, sensorPort, description];

	ajaxPOST({
		data: {
			"keys": keys,
			"values": values,
			"sensor_id": sensorId,
			"action": "update_sensor"
		}
	}, function(data) {
		closeForm();
		if (data) {
			console.log(data)
		}
	});
}

// Обновляет данные о контроле
function updateControl() {
	var controlId 			= localData.control.id;
	var controlType 		= $("#control_type").val();
	var controlPort 		= $("#control_port").val();
	var controlDescription 	= $(".control_description").val();
	var timerDelay  		= $(".timer_delay").prop('checked') * 1;
	var timerEnable 		= $(".timer_enable").prop('checked') * 1;
	var timerHours 			= $(".timer_input_hours").val();
	var timerMinutes 		= $(".timer_input_minutes").val();
	var keys 				= ["type", "port", "description", "timer_enable", "timer_hours", "timer_minutes", "timer_delay"];
	var values 				= [controlType, controlPort, controlDescription, timerEnable, timerHours, timerMinutes, timerDelay];

	ajaxPOST({
		data: {
			"keys": keys,
			"values": values,
			"control_id": controlId,
			"action": "update_control"
		}
	}, function(data) {
		closeForm();
		if (data) {
			console.log(data);
		}
	});
}

function togleTimer() {
	if ($(".timer_enable").prop('checked')) {
		$('.timer_time').show();
	} else {
		$('.timer_time').hide();
	}
}

// Закрывает форму добавления-редактирования
function closeForm() {
	$(".form_wrapper").hide();
	$(".form").hide();
	$(".finish_buttons input").hide();
}

// Показывает форму добавления-редактирования помещения
// Аргументы:
// mode 	   - режим формы, добавление или редактироване
// id 		   - идентификатор редактируемого помещения
// description - описание помещения
function showRoomForm(mode, id, description) {
	var formWrapper 	= $(".form_wrapper");
	var roomForm 		= $(".room_form");
	var saveRoomButtton = $("#save_room");
	var addRoomButton 	= $("#add_room");

	switch (mode) {
		case "add":
			$(addRoomButton).show();
		break;
		case "edit":
			$(saveRoomButtton).show();
			localData.room.id = id;
			$(".room_description").val(description);
		break;
	}

	$(formWrapper).show();
	$(roomForm).show();
}

// Показывает форму для добавления помещения
function showRoomAddForm() {
	showRoomForm("add");
}

// Показывает форму для редактирования помещения
function showRoomEditForm() {
	var roomE 		= $(this).parent().parent();
	var roomId 		= $(roomE).attr("data-room-id");
	var description = $(roomE).attr("data-room-description");
	showRoomForm("edit", roomId, description);
}

// Показывает форму для редактирования-добавления параметра
// mode - режим формы, добавление или редактирование
// id - идентификатор редактируемого параметра, или идентификатор помещения
// type - тип параметра
// targetValue - целевое значение параметра
function showParamForm(mode, id, type, targetValue) {
	var formWrapper 	= $(".form_wrapper");
	var paramForm 		= $(".param_form");
	var saveParamButton = $("#save_param");
	var addParamButton 	= $("#add_param");

	switch (mode) {
		case "add":
			$(addParamButton).show();
			localData.param.roomId = id;
		break;
		case "edit":
			$(saveParamButton).show();
			localData.param.id = id;
			$("#param_type").val(type);
			$("#target_value").val(targetValue);
		break;
	}

	$(formWrapper).show();
	$(paramForm).show();
}

// Показывает форму для добавления параметра
function showParamAddForm() {
	var roomId = $(this).parent().parent().attr("data-room-id");
	showParamForm("add", roomId);
}

// Показывает форму для редактирования параметра
function showParamEditForm() {
	var paramE 		= $(this).parent().parent();
	var paramId 	= $(paramE).attr("data-param-id");
	var type 		= $(paramE).attr("data-param-type");
	var targetValue = $(paramE).attr("data-param-target-value");

	showParamForm("edit", paramId, type, targetValue);
}

// Показывает форму для добавления-редактирования датчика
// Аргументы:
// mode - режим форму, добавление или редактирование
// id 	- идентификатор редактируемого датчика, или идентификатор параметра
// type - тип датчика
// port - порт датчика
function showSensorForm(mode, id, type, port, description) {
	var formWrapper 		= $(".form_wrapper");
	var sensorForm 			= $(".sensor_form");
	var addSensorButton 	= $("#add_sensor");
	var saveSensorButton 	= $("#save_sensor");

	switch (mode) {
		case "add":
			$(addSensorButton).show();
			localData.sensor.paramId = id;
		break;
		case "edit":
			$(saveSensorButton).show();
			localData.sensor.id = id;
			$("#sensor_type").val(type);
			$("#sensor_port").val(port);
			$(".sensor_description").val(description);
		break;
	}

	$(formWrapper).show();
	$(sensorForm).show();
}

// Показывает форму для добавления датчика
function showSensorAddForm() {
	var paramId = $(this).parent().parent().attr("data-param-id");
	showSensorForm("add", paramId);
}

// Показывает форму для редактирования датчика
function showSensorEditForm() {
	var sensorE 	= $(this).parent().parent();
	var sensorId 	= $(sensorE).attr("data-sensor-id");
	var sensorType 	= $(sensorE).attr("data-sensor-type");
	var sensorPort 	= $(sensorE).attr("data-sensor-port");
	var description = $(sensorE).attr("data-sensor-description");
	showSensorForm("edit", sensorId, sensorType, sensorPort, description);
}

// Показывает форму для добавления-редактированя контрола
// Аргументы:
// mode - режим формы, добавление или редактирование
// id - идентификатор редактируемого контрола, или идентификатор параметра
function showControlForm(mode, id, type, port, description, timerEnable, timerHours, timerMinutes, timerDelay) {
	var formWrapper 		= $(".form_wrapper");
	var controlForm 		= $(".control_form");
	var addControlButton 	= $("#add_control");
	var editControlButton 	= $("#save_control");
	timerDelay *= 1;
	timerEnable *= 1;

	switch (mode) {
		case "add":
			$(addControlButton).show();
			localData.control.paramId = id;
		break;
		case "edit":
			$(editControlButton).show();
			localData.control.id = id;
			$("#control_type").val(type);
			$("#control_port").val(port);
			$(".control_description").val(description);
			$(".timer_delay").prop("checked", timerDelay);
			$(".timer_enable").prop("checked", timerEnable);
			$(".timer_input_hours").val(timerHours);
			$(".timer_input_minutes").val(timerMinutes);

			if (timerEnable) {
				$(".timer_time").show();
			} else {
				$(".timer_time").hide();
			}
		break;
	}

	$(formWrapper).show();
	$(controlForm).show();
}

// Показывает форму для добавления контрола
function showControlAddForm() {
	var paramId = $(this).parent().parent().attr("data-param-id");
	showControlForm("add", paramId);
}

// Показывает форму для редактирования контрола
function showControlEditForm() {
	var controlE 			= $(this).parent().parent();
	var controlId 			= $(controlE).attr("data-control-id");
	var controlType 		= $(controlE).attr("data-control-type");
	var controlPort 		= $(controlE).attr("data-control-port");
	var controlDescription 	= $(controlE).attr("data-control-description");
	var timerDelay  		= $(controlE).attr("data-control-timer-delay");
	var timerEnable 		= $(controlE).attr("data-control-timer-enable");
	var timerHours 			= $(controlE).attr("data-control-timer-hours");
	var timerMinutes 		= $(controlE).attr("data-control-timer-minutes");

	showControlForm("edit", controlId, controlType, controlPort, controlDescription, timerEnable, timerHours, timerMinutes, timerDelay);
}

// Возвращает класс для параметра по типу параметра
// Аругменты:
// type - тип параметра
function getParamClassByType(type) {
	var className = 0;
	type = type * 1;

	switch (type) {
		case 1:
			className = 'param-hum';
		break;
		case 2:
			className = 'param-tmp';
		break;
		case 3:
			className = 'param-co2';
		break;
	}

	return className;
}
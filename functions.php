<?php

	/*
	 * Система упрвления климатом. Серверная часть
	 * Алтухов Илья
	 */

	include $_SERVER["DOCUMENT_ROOT"] . "/config.php";

	session_start();
	header("Content-Type: text/html; charset=utf8");

	define(PARAM_PART, 0);
	define(SENSOR_PART, 1);
	define(CONTROL_PART, 2);
	define(MEMMORY_PART, 3);

	// Возвращает соединение к базе
	function myLink() {
		GLOBAL $db_host;
		GLOBAL $db_user;
		GLOBAL $db_pass;
		GLOBAL $db_name;

		return mysqli_connect($db_host, $db_user, $db_pass, $db_name);
	}

	// Экранирует небезопасные символы
	// Аргументы:
	// $arr - ассоциативный массив или строка содержащие значения для экранирования
	function escape($arr) {
		$link = myLink();

		if (is_array($arr)) {
			foreach ($arr as $key => $value) {
				if (is_array($value)) {
					$arr[$key] = escape($value);
				} else {
					$arr[$key] = "'" . mysqli_real_escape_string($link, $value) . "'";
				}
			}
		} else {
			$arr = mysqli_real_escape_string($link, $arr);
			$arr = "'" . $arr . "'";
		}

		return $arr;
	}

	// Проверяет входит лю ключ в список разрешенных
	// Аргументы:
	// $keys 	   - ключ или массив ключей для проверки
	// $validKeys  - список разрешенных ключей
	function isKeyValid($keys, $validKeys) {

		if (is_array($keys)) {
			foreach ($keys as $key) {
				if (!in_array($key, $validKeys)) return false;
			}
		} else {
			if (!in_array($keys, $validKeys)) return false;
		}

		return true;
	}

	// Производит вставку данных в базу
	// Аргументы:
	// $keys 	 - строка с ключем или массив ключей с названиями ключей таблицы
	// $values   - значение или массив значений
	// $table 	 - название таблицы
	function insertQuery($keys, $values, $table) {

		$link 	= myLink();
		$values = escape($values);
		$query 	= "INSERT INTO " . $table;
		$query .= " (";

		if (is_array($keys)) {
			foreach ($keys as $k => $v) {
				$keys[$k] = "`" . $v . "`";
			}

			$query .= implode(",", $keys);
		} else {
			$query .= "`" . $keys . "`";
		}

		$query .= ") VALUES (";

		if (is_array($values)) {
			$query .= implode(",", $values);
		} else {
			$query .= $values;
		}

		$query .= ");";
		$res = mysqli_query($link, $query);

		return $res;
	}

	// Возвращает данные из баз
	// Аргументы:
	// $keys  - ключ или массив ключей таблицы
	// $table - название таблицы
	// $id    - идентификатор строки, если не установлен возвращает все записи
	function selectQuery($keys, $table, $id = NULL) {
		$link = myLink();
		if (is_array($keys)) {
			$query = "SELECT " . implode(",", $keys);
		} else {
			$query = "SELECT " . $keys;
		}

		$query .= " FROM " . $table;

		if (!is_null($id)) {
			$query .= " WHERE id=" . $id;
		}

		$query .= ";";
		$res = mysqli_query($link, $query);

		return $res;
	}

	// Выбирает значение по названию из информационной таблицы
	function selectFromInfoTable($key) {
		$link = myLink();
		$query = "SELECT name, value FROM info WHERE name = '$key' LIMIT 1;";

		$res = mysqli_query($link, $query);

		if ($res) {
			while ($row = mysqli_fetch_assoc($res)) {
				return $row['value'];
			}
		}

		return false;
	}

	// Возвращает количество зописей в таблицу
	// Аргументы:
	// $table - название таблицы
	function getNumRows($table) {
		$query 	= "SELECT id FROM " . $table . ";";
		$link 	= myLink();
		mysqli_query($link, $query);

		return mysqli_affected_rows($link);
	}

	// Обновляет записись в базе по идентификатору
	// Аргументы:
	// $key   - ключ массив ключей
	// $value - значение или массив значений
	// $table - название таблицы
	// $id    - идентификатор записи
	function updateQuery($keys, $values, $table, $id) {
		$values 		= escape($values);
		$id 		   *= 1;
		$link 			= myLink();
		$keysCount 		= count($keys);
		$valuesCount 	= count($values);

		if ($keysCount != $valuesCount) {
			return false;
		} else {
			$query 	= "UPDATE " . $table;
			$query .= " SET ";

			for ($i = 0; $i < $keysCount; $i++) {
				$query .= "`" . $keys[$i] . "`" . "=" . $values[$i];
				if ($i < $keysCount - 1) {
					$query .= ", ";
				}
			}
			$query .= " WHERE id=" . $id . ";";
		}

		$res = mysqli_query($link, $query);

		return $res;
	}

	// Обновляет запись в базе по ключу
	// Аргументы:
	// $key   - ключ
	// $value - значение
	// $table - название таблицы
	function updateQueryByKey($key, $value, $table) {
		$value 	= escape($value);
		$link 	= myLink();

		if (!is_array($key) || !is_array($value)) {
			$query = "UPDATE " . $table . " SET ";
			$query .= $key . "=" . $value;
		}

		$res = mysqli_query($link, $query);
	}

	// Обновляет опцию в таблице с информацией
	// Аргументы:
	// $key   - название ключа
	// $value - занчение
	function updateInfoTable($key, $value) {
		$link = myLink();
		$query = "UPDATE info SET `value` = '$value' WHERE name = '$key';";

		mysqli_query($link, $query);
	}

	// Обновляет одну или несколько записей в базе
	// Аргументы:
	// $keys   - ключ или массив ключей
	// $values - значение или массив значений
	// $table  - название таблицы
	function updateQueryMany($keys, $values, $table) {
		$values = escape($values);
		$link 	= myLink();

		if (is_array($keys)) {
			$keysCount = count($keys);
		} else {
			$keysCount = 1;
			$keys = array($keys);
		}

		$value_count = count($values);
		array_unshift($keys, "id");
		$query = "INSERT INTO " . $table;
		$query .= "(" . implode(",", $keys) . ") VALUES ";

		for ($i = 0; $i < $value_count; $i++) {
			$query .= "(" . implode(",", $values[$i]) . ")";
			if ($i < $value_count - 1) {
				$query .= ",";
			}
		}

		array_shift($keys);
		$query .= " ON DUPLICATE KEY UPDATE ";

		for ($i = 0; $i < $keysCount; $i++) {
			$query .=  $keys[$i] . " = VALUES(" . $keys[$i] . ")";
			if ($i < $keysCount - 1) {
				$query .= ",";
			}
		}

		$query .= ";";
		$res = mysqli_query($link, $query);

		return $res;
	}

	// Удаляет запись из базы
	// Аргументы:
	// $table - название таблицы
	// $id    - идентификатор записи
	function deleteQuery($table, $id) {
		$id 	*= 1;
		$link 	= myLink();
		$query 	= "DELETE FROM ";
		$query .= $table;
		$query .= " WHERE id=" . $id . ";";
		$res 	= mysqli_query($link, $query);

		return $res;
	}

	// Расчитывает контрольную сумму по алгоритму CRC16
	// Аргументы:
	// $string - исходная строка
	function crc16($string) {
		$string 	.= '';
		$def 		= 0xffff;
		$crc 		= $def;
		$len 		= strlen($string);
		$poly 		= 0x1021;
		$i 			= 0;

		while ($len--) {
			$crc ^= ord($string[$i++]) << 8;
			$crc &= $def;

			for ($j = 0; $j < 8; $j++) {
				if($crc & 0x8000) {
					$crc <<= 1;
					$crc ^= $poly;
				} else {
					$crc <<= 1;
				}
				$crc &= $def;
			}
		}
		return $crc;
	}

	// Добалвяет помещение в базу
	// Аргументы:
	// $description - название помещения
	function addRoom($description) {
		if ($_SESSION['logged']) {
			return insertQuery("description", $description, "rooms");
		}
	}

	// Добавляет параметр в базу
	// Аргументы:
	// $roomId 		- идентификатор помещения
	// $type 		- тип параметра
	// $targetValue - целевое значение параметра
	function addParam($roomId, $type, $targetValue) {
		$keys 	= array("room_id", "type", "target_value");
		$values = array($roomId, $type, $targetValue);
		
		if ($_SESSION['logged']) {
			updateHash("params_hash");
			updateInfoTable("params_loaded", 0);
			return insertQuery($keys, $values, "params");
		}
	}

	// Добавляет датчик в базу
	// $paramId - идентификатор параметра
	// $type    - тип датчика
	// $port    - порт датчика
	function addSensor($paramId, $type, $port, $description) {
		$keys 	= array("param_id", "type", "port", "description");
		$values = array($paramId, $type, $port, $description);
		
		if ($_SESSION['logged']) {
			updateHash("sensors_hash");
			updateInfoTable("sensors_loaded", 0);
			return insertQuery($keys, $values, "sensors");
		}
	}

	// Добавляет контрол в базу
	// Аргументы:
	// $paramId 	- идентификатор параметра
	// $type 		- тип контрола
	// $port 		- порт контрола
	// $description - название контрола
	function addControl($paramId, $type, $port, $description, $timerEnable, $timerHours, $timerMinutes, $timerDelay) {
		$startTime 	= time();
		$endTime 	= $startTime + ($timerHours * 3600) + ($timerMinutes * 60);
		$keys 		= array("param_id", "type", "port", "description", "timer_enable", "timer_start", "timer_end", "timer_delay");
		$values 	= array($paramId, $type, $port, $description, $timerEnable, $startTime, $endTime, $timerDelay);
		
		if ($_SESSION['logged']) {
			updateHash("controls_hash");
			updateInfoTable("controls_loaded", 0);
			return insertQuery($keys, $values, "controls");
		}
	}

	// Обновляет данные о помещении
	// $key   - ключ
	// $value - значение
	// $id    - идентификатор помещения
	function updateRoom($key, $value, $id) {
		$res 		= false;
		$validKeys 	= array("description");

		if (isKeyValid($key, $validKeys) && $_SESSION['logged']) {
			$res = updateQuery($key, $value, "rooms", $id);
		}
		
		return $res;
	}

	// Обновляет данные о параметре
	// Аргументы:
	// $keys   - ключ или массив ключей
	// $values - значение или массив занчений
	// $id     - идентификатор параметра
	function updateParam($keys, $values, $id) {
		$res 		= false;
		$validKeys 	= array("type", "target_value");

		if (isKeyValid($keys, $validKeys) && $_SESSION['logged']) {
			updateHash("params_hash");
			updateInfoTable("params_loaded", 0);
			$res = updateQuery($keys, $values, "params", $id);
		}

		return $res;
	}

	// Обновляет данные о датчике
	// Аргументы:
	// $keys   - ключ или массив ключей
	// $values - значение или массив значений
	// $id     - идентификатор датчика
	function updateSensor($keys, $values, $id) {
		$res 		= false;
		$validKeys 	= array("type", "value", "port", "description");

		if (isKeyValid($keys, $validKeys) && $_SESSION['logged']) {
			updateHash("sensors_hash");
			updateInfoTable("sensors_loaded", 0);
			$res = updateQuery($keys, $values, "sensors", $id);
		}

		return $res;
	}

	// Обновляет данные о контроле
	// $keys   - ключ или массив ключей
	// $values - значение или массив значений
	// $id     - идентификатор контрола
	function updateControl($keys, $values, $id) {
		$res 		= false;
		$validKeys 	= array("type", "current_state", "port", "description", "timer_enable", "timer_start", "timer_end", "timer_delay");
		$count 		= count($keys);
		$startTime 	= time();
		$endTime;
		$hours;
		$minutes;

		for ($i = 0; $i < $count; $i++) {
			if ($keys[$i] == "timer_hours") {
				$hours 		= $values[$i];
				$keys[$i] 	= "timer_start";
				$values[$i] = $startTime;
			}

			if ($keys[$i] == "timer_minutes") {
				$minutes 	= $values[$i];
				$keys[$i] 	= "timer_end";
				$endTime 	= $startTime + ($hours * 3600) + ($minutes * 60);
				$values[$i] = $endTime;
			}
		}
		if (isKeyValid($keys, $validKeys) && $_SESSION['logged']) {
			updateHash("controls_hash");
			updateInfoTable("controls_loaded", 0);
			$res = updateQuery($keys, $values, "controls", $id);
		}

		return $res;
	}

	// Возвращает количество параметров в базе
	function getNumParams() {
		return  getNumRows("params");
	}

	// Возвращает количество датчиков в базе
	function getNumSensors() {
		return getNumRows("sensors");
	}

	// Возвращает количество контролов в базе
	function getNumControls() {
		return getNumRows("contols");
	}

	// Возвращает массив помещений
	function getAllRooms() {
		$keys 	= array("id", "description");
		$res 	= selectQuery($keys, "rooms");
		$rooms 	= array();

		if ($res) {
			while ($row = mysqli_fetch_row($res)) {
				array_push($rooms, array(
						"id" => $row[0],
						"description" => $row[1],
						"params" => array()
					));
			}
		} else {
			return $res;
		}

		return $rooms;
	}

	// Возвращает массив параметров
	function getAllParams() {
		$query 	= "SELECT params.id, params.room_id, params.type, params.target_value, params.current_value FROM params;";
		$link 	= myLink();
		$res 	= mysqli_query($link, $query);
		$params = array();

		if ($res) {
			while ($row = mysqli_fetch_row($res)) {
				array_push($params, array(
						"id" => $row[0],
						"room" => $row[1],
						"sistem" => $row[0],
						"type" => $row[2],
						"target_value" => $row[3],
						"current_value" => $row[4],
						"parent_id" => $row[1],
						"sensors" => array(),
						"controls" => array()
					));
			}
		} else {
			return $res;
		}

		return $params;
	}

	// Возвращает строку параметров, подготовленную для контроллера
	// Аргументы:
	// $addHash - указывает, добавлять ли в конце строки контрольную сумму
	function getAllParamsForController($addHash = true) {
		$query 	= "SELECT params.id, params.room_id, params.type, params.target_value FROM params;";
		$link 	= myLink();
		$res 	= mysqli_query($link, $query);
		$params = array();

		if ($res) {
			while ($row = mysqli_fetch_row($res)) {
				if ($row[0] && $row[1]) {
					array_push($params, array(
						"id" => $row[0],
						"room_id" => $row[1],
						"type" => $row[2],
						"target_value" => $row[3]
					));
				}
			}
		} else {
			return $res;
		}
		
		$params = serializeData($params, $addHash);
		updateInfoTable("params_loaded", 1);

		return $params;
	}

	// Возвращает контрльную сумму строки параметров
	function getParamsHash() {
		return selectFromInfoTable("params_hash");
	}

	// Возвращает массив датчиков
	function getAllSensors() {
		$query = 
			"SELECT sensors.id, sensors.param_id, sensors.type, sensors.port, sensors.value, sensors.description
			FROM sensors;";
		$link 		= myLink();
		$res 		= mysqli_query($link, $query);
		$sensors 	= array();

		if ($res) {
			while ($row = mysqli_fetch_row($res)) {
				array_push($sensors, array(
						"id" => $row[0],
						"type" => $row[2],
						"port" => $row[3],
						"value" => $row[4],
						"description" => $row[5],
						"parent_id" => $row[1]
					));
			}
		} else {
			return $res;
		}

		return $sensors;
	}

	// Возвращает строку датчиков, подготовленную для контроллера
	// Аргументы:
	// $addHash - указывает, добавлять ли в конце строки контрольную сумму
	function getAllSensorsForController($addHash = true) {
		$query 		= "SELECT sensors.id, sensors.param_id, sensors.type, sensors.port FROM sensors;";
		$link 		= myLink();
		$res 		= mysqli_query($link, $query);
		$sensors 	= array();

		if ($res) {
			while ($row = mysqli_fetch_row($res)) {
				if ($row[0] && $row[1] && $row[2] && $row[3]) {
					array_push($sensors, array(
							"id" => $row[0],
							"param_id" => $row[1],
							"type" => $row[2],
							"port" => $row[3]
						));
				}
			}
		} else {
			return $res;
		}

		$sensors = serializeData($sensors, $addHash);
		updateInfoTable("sensors_loaded", 1);

		return $sensors;
	}

	// Возвращает контрольную сумму строки датчиков
	function getSensorsHash() {
		return selectFromInfoTable("sensors_hash");
	}

	// Возвращает массив котролов
	function getAllControls() {
		$query = 
			"SELECT controls.id, controls.param_id, controls.type, controls.current_state, controls.port, controls.description, controls.timer_enable, controls.timer_start, controls.timer_end, controls.timer_delay
			FROM controls;";
		$link 		= myLink();
		$res 		= mysqli_query($link, $query);
		$controls 	= array();

		if ($res) {
			while ($row = mysqli_fetch_assoc($res)) {
				$end 	= $row['timer_end'];

				if ($row['timer_delay'] * 1) {
					$start 	= $row['timer_start'];
				} else {
					$start 	= time();
				}
				
				if ($start >= $end) {
					$timerHours 	= 0;
					$timerMinutes 	= 0;
					$timerSeconds 	= 0;
				} else {
					$offset 		= $end - $start;
					$timerHours 	= floor($offset / 3600);
					$timerMinutes 	= floor(($offset - ($timerHours * 3600)) / 60);
					$timerSeconds 	= $offset - ($timerHours * 3600 + $timerMinutes * 60);
				}

				array_push($controls, array(
						"id" => $row['id'],
						"type" => $row['type'],
						"current_state" => $row['current_state'],
						"port" => $row['port'],
						"description" => $row['description'],
						"timerEnable" => $row['timer_enable'],
						"timerDelay" => $row['timer_delay'],
						"timerHours" => $timerHours,
						"timerMinutes" => $timerMinutes,
						"timerSeconds" => $timerSeconds,
						"parent_id" => $row['param_id']
					));
			}
		} else {
			return $res;
		}

		return $controls;
	}

	// Возвращает строку контролов, подготовленную для контроллера
	// Аргументы:
	// $addHash - указывает, добавлять ли в конце строки контрольную сумму
	function getAllControlsForController($addHash = true) {
		$query 		= "SELECT controls.id, controls.param_id, controls.type, controls.port, controls.timer_enable, controls.timer_start, controls.timer_end, controls.timer_delay FROM controls;";
		$link 		= myLink();
		$res 		= mysqli_query($link, $query);
		$controls 	= array();

		if ($res) {
			while ($row = mysqli_fetch_row($res)) {
				if ($row[0] && $row[1] && $row[2] && $row[3]) {
					array_push($controls, array(
							"id" => $row[0],
							"param_id" => $row[1],
							"type" => $row[2],
							"port" => $row[3],
							"timer_enable" => $row[4],
							"timer_start" => $row[5],
							"timer_end" => $row[6],
							"timer_delay" => $row[7]
						));
				}
			}
		} else {
			return $res;
		}

		$controls = serializeData($controls, $addHash);
		updateInfoTable("controls_loaded", 1);

		return $controls;
	}

	// Возвращает актуалльные контролы
	function getSourceControls() {
		$query 		= "SELECT controls.param_id, controls.id, controls.timer_delay, controls.timer_start, controls.timer_end FROM controls WHERE timer_enable = 1 AND timer_delay = 1";
		$link 		= myLink();
		$res 		= mysqli_query($link, $query);
		$controls 	= array();

		if ($res) {
			while ($row = mysqli_fetch_assoc($res)) {
				if ($row['param_id']) {
					array_push($controls, array(
						'id' => $row['id'],
						'timer_delay' => $row['timer_delay'],
						'timer_start' => $row['timer_start'],
						'timer_end' => $row['timer_end']
					));
				}
			}
		}

		return $controls;
	}

	// Возвращает контрольную сумму строки контроллеров
	function getControlsHash() {
		return selectFromInfoTable("controls_hash");
	}

	// Возворащает строку с контрольными суммами параметров, датчиков и контролов
	function getHashs() {
		$hashs = getParamsHash() . ":" . getSensorsHash() . ":" . getControlsHash();
		$hashs .= ";" . crc16($hashs);

		return $hashs;
	}

	function updateHash($hashName) {
		$hash = selectFromInfoTable($hashName) * 1;
		$newHash = rand(1000, 65535);

		while ($has == $newHash) {
			$newHash = rand(1000, 65535);
		}

		updateInfoTable($hashName, $newHash);
	}

	// Сериализует данные для отправки на контроллер
	// Аргументы:
	// $arr     - массив с данными
	// $addHash - указывает, нужно ли добавлять контрольную сумму
	function serializeData($arr, $addHash) {
		$string 	= "";
		$itemsCount = count($arr);
		$propsCount;

		for ($i = 0; $i < $itemsCount; $i ++) {
			$propsCount = count($arr[$i]);
			$j = 0;

			foreach ($arr[$i] as $prop) {
				$string .= $prop;

				if ($j < $propsCount - 1) {
					$string .= ",";
				}

				$j++;
			}

			if ($i < $itemsCount - 1) {
				$string .= ":";
			}
		}

		if ($addHash) {
			$string .= ";" . crc16($string);
		}
		
		return $string;
	}

	// Возвращает строку тегами, по которым контроллер понимает, где находится тело ответа
	function prepareDataForController($str) {
		$str = "--data\n" . $str . "\ndata--\n";
		
		return $str;
	}

	// Удаляет помещение
	// Аргументы:
	// $id - идентификатор комнаты
	function deleteRoom($id) {
		if ($_SESSION['logged']) {
			return deleteQuery("rooms", $id);
		}
	}

	// Удаляет парметр
	// Аргументы:
	// $id - идентификатор параметра
	function deleteParam($id) {
		if ($_SESSION['logged']) {
			updateHash("params_hash");
			updateInfoTable("params_loaded", 0);
			return deleteQuery("params", $id);
		}
	}

	// Удаляет датчик
	// Аргументы:
	// $id - идентификатор датчика
	function deleteSensor($id) {
		if ($_SESSION['logged']) {
			updateHash("sensors_hash");
			updateInfoTable("sensors_loaded", 0);
			return deleteQuery("sensors", $id);
		}
	}

	// Удаляет контрол
	// Аргументы:
	// $id - идентификатор контрола
	function deleteControl($id) {
		if ($_SESSION['logged']) {
			updateHash("controls_hash");
			updateInfoTable("controls_loaded", 0);
			return deleteQuery("controls", $id);
		}
	}

	// Присоединяет дочерние элементы к родительским
	// Например: контролы и датчики к параметрам, параметры к помещениям
	// Аргументы:
	// &$children  - ссылка на массив с дочерними элементами
	// &$parent    - ссылка на массив с родительскими элементами
	// $child_cont - навзание контейнера в родительском элементе для дочерних элементов
	function insertToParent(&$children, &$parent, $child_cont) {
		$isArray 	= is_array($children) && is_array($parent);
		$isNull 	= is_null($children) || is_null($parent) || is_null($child_cont);

		if (!$isArray || $isNull) {
			return;
		}
		
		foreach ($parent as $parKey => $par) {
			foreach ($children as $childKey => $child) {
				if ($parent[$parKey]["id"] == $child["parent_id"]) {
					array_push($parent[$parKey][$child_cont], $child);
					unset($children[$child_key]);
					sort($children);
				}
			}
		}
	}

	// Возвращает структуру база (помещения, параметра, датчики и контролы)
	function getStructure() {
		$rooms 		= getAllRooms();
		$params 	= getAllParams();
		$sensors 	= getAllSensors();
		$controls 	= getAllControls();

		insertToParent($sensors, $params, "sensors");
		insertToParent($controls, $params, "controls");
		insertToParent($params, $rooms, "params");
		
		return $rooms;
	}

	// Возвращает количество секунд с последнего обновления данных контроллером
	function getTimeStamps() {
		$lastUpdate = selectFromInfoTable("last_update");

		if (!$lastUpdate) $lastUpdate = 0;

		return array(
				"last_update" => $lastUpdate,
				"timestamp" => time()
			);
	}

	// Возвращает количество свободной памяти в контроллере
	function getFreeMemory() {
		$memory = selectFromInfoTable("memory");

		if (!$memory) $memory = "Неизвестно";

		return $memory;
	}

	// Возвращает всю необходимую информацию для мониторинга
	function getInfo() {
		$structure 	= getStructure();
		$timeStamps = getTimeStamps();
		$freeMemory = getFreeMemory();

		return array(
			'structure' => $structure,
			'timeStamps' => $timeStamps,
			'freeMemory' => $freeMemory
		);
	}

	// Обновляет данные в базе (значения датчиков, состояния контролов и т.д.)
	// Аргументы:
	// $data строка с данными от контроллера
	function insertData($data) {
		if (!checkString($data)) {
			return;
		}

		$sourceControls 	= getSourceControls();
		$controlsForUpdate 	= array();
		$data 				= getDatafromString($data);
		$params 			= getRecord($data, PARAM_PART);
		$sensors 			= getRecord($data, SENSOR_PART);
		$controls 			= getRecord($data, CONTROL_PART);
		$freeMemory 		= getRecord($data, MEMMORY_PART);
		$paramsLoaded 		= selectFromInfoTable("params_loaded") * 1;
		$sensorsLoaded 		= selectFromInfoTable("sensors_loaded") * 1;
		$controlsLoaded 	= selectFromInfoTable("controls_loaded") * 1;
		$sourceControlKeys 	= array("timer_delay", "timer_start", "timer_end");
		$timerStart 		= time();
		$timerEnd 			= 0;

		foreach ($sourceControls as $sourceControl) {
			foreach ($controls as $control) {
				if ($sourceControl['id'] == $control[0]) {
					if ($sourceControl['timer_delay'] == 1 && $control[2] == 0) {
						$offset = $sourceControl['timer_end'] - $sourceControl['timer_start'];
						$timerEnd = $timerStart + $offset;
						array_push($controlsForUpdate, array(
							$sourceControl['id'],
							0,
							$timerStart,
							$timerEnd
						));
					}
				}
			}
		}

		if ($paramsLoaded) updateQueryMany("current_value", $params, "params");
		if ($sensorsLoaded) updateQueryMany("value", $sensors, "sensors");
		if ($controlsLoaded) {
			updateQueryMany(array("current_state", "timer_delay"), $controls, "controls");
			updateQueryMany($sourceControlKeys, $controlsForUpdate, "controls");
		}

		updateInfoTable("last_update", time());
		updateInfoTable("memory", $freeMemory);

		logToFile($data);
	}

	// Возвращает строку без контрольной суммы
	// Аргументы:
	// $str - строка с данными от котроллера
	function getDatafromString($str) {
		$str = explode(";", $str);
		$str = $str[0];

		return $str;
	}

	// Вовзращает контрольную сумму
	// Аргументы:
	// $str - строка с данными от котроллера
	function getHashFromString($str) {
		$str = explode(";", $str);
		$str = $str[1];

		return $str;
	}

	// Проверяет строку на корректность
	// Аргументы:
	// // $str - строка с данными от котроллера
	function checkString($str) {
		$data = getDatafromString($str);
		$hash = getHashFromString($str);

		return $hash == crc16($data);
	}

	// Возвращает массив параметров, датчиков или контролов
	// $data       - исходная строка от контроллера
	// $partNumber - номер части в которой находится данные
	function getRecord($data, $partNumber) {
		$data = explode("|", $data);
		$data = $data[$partNumber];

		if (strpos($data, ":") !== false || strpos($data, ",") !== false) {
			$data = explode(":", $data);

			foreach ($data as $k => $v) {
				$data[$k] = explode(",", $v);
			}
		}

		return $data;
	}

	// Записывает данные пришедшие с контроллера в файл
	// $data - сериализованные данные пришедшие с контроллера
	function logToFile($data) {
		$fileName 	= 'logs/log-' . date('Y-m-d') . '.txt';
		$data 		= date('Y-m-d-H:i:s') . ' ' . $data . "\n";

		if (file_exists($fileName)) {
			file_put_contents($fileName, $data, FILE_APPEND);
		} else {
			file_put_contents($fileName, $data);
		}
	}
?>
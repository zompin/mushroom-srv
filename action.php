<?php

	/*
	 * Система упрвления климатом. Серверная часть
	 * Алтухов Илья
	 */

	include $_SERVER["DOCUMENT_ROOT"] . "/functions.php";

	$action = $_GET["action"];
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		switch ($action) {
			case "get_params":
				echo prepareDataForController(getAllParamsForController());
			break;
			case "get_sensors":
				echo prepareDataForController(getAllSensorsForController());
			break;
			case "get_controls":
				echo prepareDataForController(getAllControlsForController());
			break;
			case "get_hashs":
				echo prepareDataForController(getHashs());
			break;
			case "getTimeStamps":
				echo json_encode(getTimeStamps());
			break;
			case "getFreeMemory":
				echo json_encode(getFreeMemory());
			break;
			case "getInfo":
				echo json_encode(getInfo());
			break;
			default:
				echo json_encode(getStructure());
		}
	} else {
		$res = new stdClass;
		$data = $_POST;
		switch ($data['action']) {
			case "delete_room":
				$res->status 			= deleteRoom($data["id"]);
				$res->successMessage 	= "Помещение удалено";
				$res->failMessage 		= "Не удалось удалить помещение";
			break;
			case "delete_param":
				$res->status 			= deleteParam($data["id"]);
				$res->successMessage 	= "Параметр удален";
				$res->failMessage 		= "Не удалось удалить параметр";
			break;
			case "delete_sensor":
				$res->status 			= deleteSensor($data["id"]);
				$res->successMessage 	= "Датчик удален";
				$res->failMessage 		= "Не удалос удалить датчик";
			break;
			case "delete_control":
				$res->status 			= deleteControl($data["id"]);
				$res->successMessage 	= "Устройство удалено";
				$res->failMessage 		= "Не удалось удалить устройство";
			break;
			case "add_room":
				$res->status 			= addRoom($data["description"]);
				$res->successMessage 	= "Помещение добавлено";
				$res->failMessage 		= "Не удалось добавить помещение";
			break;
			case "add_param":
				$res->status 			= addParam($data["room_id"], $data["type"], $data["target_value"]);
				$res->successMessage 	= "Параметр добавлен";
				$res->failMessage 		= "Не удалось добавить парметр";
			break;
			case "add_sensor":
				$res->status 			= addSensor($data["param_id"], $data["type"], $data["port"], $data["description"]);
				$res->successMessage 	= "Датчик добавлен";
				$res->failMessage 		= "Не удалось добавить датчик";
			break;
			case "add_control":
				$res->status 			= addControl($data["param_id"], $data["type"], $data["port"], $data["description"], $data['timerEnable'], $data['timerHours'], $data['timerMinutes'], $data['timerDelay']);
				$res->successMessage 	= "Устройство добавлено";
				$res->failMessage 		= "Не удалось добавить устройство";
			break;
			case "update_room":
				$res->status 			= updateRoom($data["keys"], $data["values"], $data["room_id"]);
				$res->successMessage 	= "Помещение изменено";
				$res->failMessage 		= "Не удалось изменить помещение";
			break;
			case "update_param":
				$res->status 			= updateParam($data["keys"], $data["values"], $data["param_id"]);
				$res->successMessage 	= "Параметр изменен";
				$res->failMessage 		= "Не удалось изменить параметр";
			break;
			case "update_sensor":
				$res->status 			= updateSensor($data["keys"], $data["values"], $data["sensor_id"]);
				$res->successMessage 	= "Датчик изменен";
				$res->failMessage 		= "Не удалось изменить датчик";
			break;
			case "update_control":
				$res->status 			= updateControl($data["keys"], $data["values"], $data["control_id"]);
				$res->successMessage 	= "Устройство обновлено";
				$res->failMessage 		= "Не удалось обновить устройство";
			break;
			case "insert_data":
				insertData($_POST["data"]);
			break;
			default:
				$res->status 		= false;
				$res->failMessage 	= "Неизвестное действие";
		}
		$res->action = $data["action"];
		
		echo json_encode($res);
		echo "\n";
	}
?>